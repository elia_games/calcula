from mathematics import Matrix


class JsonFormatter:
    def dump_matrix(self, matrix):
        return {
            'rows': matrix.rows,
            'columns': matrix.columns,
            'values': matrix.values
        }

    def load_matrix(self, data):
        matrix = Matrix()
        matrix.rows = data['rows']
        matrix.columns = data['columns']
        matrix.values = data['values']
        return matrix
