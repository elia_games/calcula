import argparse
import random

import os
import threading

import time

import sys

from client import CalculaClient
from mathematics import Matrix


class ClientTask:
    def __init__(self):
        self.name = ''
        self.id = ''
        self.status = ''
        self.timestamp = 0.0
        self.duration = 0.0


def create_matrix(rows, columns):
    matrix = Matrix()
    matrix.rows = rows
    matrix.columns = columns
    matrix.values = [random.randint(0, 9) for _ in range(rows * columns)]
    return matrix


def write_matrix(matrix, path):
    os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, "w") as file:
        file.write('{}x{}'.format(matrix.rows, matrix.columns) + os.linesep)
        for row in range(matrix.rows):
            pointer = row * matrix.columns
            values = matrix.values[pointer: pointer + matrix.columns]
            line = '\t'.join(map(lambda x: str(x), values))
            file.write(line + os.linesep)


def report_tasks_status(tasks):
    while True:
        reports = []
        for task in tasks:
            if task.status != 'success':
                task.duration = time.time() - task.timestamp
            reports += ['{} {} t={:.2f}s'.format(task.name, task.status, task.duration)]
        sys.stdout.write('\r' + ', '.join(reports))
        sys.stdout.flush()
        if all(task.status == 'success' for task in tasks):
            break
        time.sleep(1.0)


def run_demo(calcula_url):
    client_tasks = []
    client = CalculaClient(calcula_url)
    for i in range(1, 4):
        task = ClientTask()
        task.timestamp = time.time()
        task.status = 'creation'
        size = 100
        task.name = 'Task #{} x{}'.format(i, size)
        left = create_matrix(size, size)
        right = create_matrix(size, size)
        write_matrix(left, './output/task.{}.left.txt'.format(i))
        write_matrix(right, './output/task.{}.right.txt'.format(i))

        def post(t):
            t.id = client.post_multiplication_task(left, right)

        threading.Thread(target=post, args=(task,)).start()
        client_tasks.append(task)

    threading.Thread(target=report_tasks_status, args=(client_tasks,)).start()

    while not all(task.status == 'success' for task in client_tasks):
        for task in client_tasks:
            if task.id:
                task.status = client.get_multiplication_task_status(task.id)
        time.sleep(1.0)

    print()
    for index, task in enumerate(client_tasks):
        result = client.get_multiplication_task_result(task.id)
        print('{} save result to ./output/task.{}.*.txt'.format(task.name, index + 1))
        write_matrix(result, './output/task.{}.result.txt'.format(index + 1))
    print('Done.')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', default='http://127.0.0.1:5000')
    args = parser.parse_args()
    run_demo(args.host)
