import requests

from formatters import JsonFormatter


class CalculaClient:
    def __init__(self, base_url):
        self.base_url = base_url
        self.formatter = JsonFormatter()

    def post_multiplication_task(self, left, right):
        payload = {
            'left': self.formatter.dump_matrix(left),
            'right': self.formatter.dump_matrix(right)
        }
        url = self.get_url('/api/v1/matrix/multiplication/tasks')
        response = requests.post(url, json=payload)
        if response.status_code != 200:
            raise Exception(response.json())
        return response.json()['id']

    def get_multiplication_task_status(self, id):
        url = self.get_url('/api/v1/matrix/multiplication/tasks/{}/status'.format(id))
        response = requests.get(url)
        return response.json()['code']

    def get_multiplication_task_result(self, id):
        url = self.get_url('/api/v1/matrix/multiplication/tasks/{}/result'.format(id))

        response = requests.get(url)
        return self.formatter.load_matrix(response.json()['matrix'])

    def get_url(self, operation):
        return self.base_url + operation
