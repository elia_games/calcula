from functools import reduce


class Matrix:
    """
    Represents a matrix that can be partitioned
    into sections called blocks or sub-matrices.
    """

    def __init__(self, rows=0, columns=0, values=None):
        self.rows = rows
        self.columns = columns
        if values is not None:
            self.values = values
        else:
            self.values = [0] * (rows * columns)

    def get_block(self, row, column, size):
        block = Matrix()
        block.rows = size
        block.columns = size
        row_offset = row * size
        column_offset = column * size
        for row in range(size):
            pointer = (row + row_offset) * self.columns + column_offset
            block.values += self.values[pointer: pointer + size]
        return block

    def put_block(self, block, row, column, size):
        row_offset = row * size
        column_offset = column * size
        for row in range(size):
            pointer = (row + row_offset) * self.columns + column_offset
            block_pointer = row * size
            self.values[pointer: pointer + size] = block.values[block_pointer: block_pointer + size]

    def is_balanced(self, block_size):
        return (self.rows + self.columns) % block_size == 0

    def __matmul__(self, other):
        result = Matrix()
        result.rows = self.rows
        result.columns = other.columns
        for i in range(result.rows):
            for j in range(result.columns):
                value = 0
                for k in range(self.columns):
                    value += self.values[i * result.columns + k] * other.values[k * other.columns + j]
                result.values.append(value)
        return result

    def __add__(self, other):
        result = Matrix()
        result.rows = self.rows
        result.columns = self.columns
        result.values = [sum(values) for values in zip(self.values, other.values)]
        return result

    def __str__(self):
        return '{}x{} {}'.format(self.rows, self.columns, self.values)


class MatrixProcessor:
    def __init__(self, block_size=50):
        self.block_size = block_size

    def emit_multiplication_units(self, a, b):
        """
        Returns block-based dimension parameters
        and scope of sub-matrices needed to calculate result matrix block.
        """
        result_block_rows = a.rows // self.block_size
        result_block_columns = b.columns // self.block_size
        for row in range(result_block_rows):
            row_blocks = list(self.emit_row_blocks(a, row))
            for column in range(result_block_columns):
                column_blocks = list(self.emit_column_blocks(b, column))
                key = row, column
                yield key, row_blocks, column_blocks

    def build_result_matrix(self, computations, result_size):
        result = Matrix(*result_size)
        for (row, column), result_block in computations:
            result.put_block(result_block, row, column, self.block_size)
        return result

    def compute_result_block(self, key, row, column):
        matrices = [a @ b for a, b in zip(row, column)]
        return key, reduce(Matrix.__add__, matrices)

    def emit_column_blocks(self, matrix, column):
        rows = matrix.rows // self.block_size
        for row in range(rows):
            yield matrix.get_block(row, column, self.block_size)

    def emit_row_blocks(self, matrix, row):
        columns = matrix.columns // self.block_size
        for column in range(columns):
            yield matrix.get_block(row, column, self.block_size)

    def validate_multiplication(self, left, right):
        errors = []
        if not (left.is_balanced(self.block_size) and right.is_balanced(self.block_size)):
            errors.append('matrix_unbalanced')
        if left.columns != right.rows:
            errors.append('incorrect_matrix_dimension')
        return errors
