import os
from celery import Celery, group, states

from mathematics import MatrixProcessor

broker = os.environ.get('RABBITMQ_BIGWIG_URL', 'amqp://localhost')

app = Celery('tasks', broker=broker, backend=broker)

app.conf.CELERY_TASK_SERIALIZER = 'pickle'
app.conf.CELERY_RESULT_SERIALIZER = 'pickle'
app.conf.CELERY_ACCEPT_CONTENT = ['json', 'pickle']


@app.task
def multiply_matrices(left, right):
    processor = MatrixProcessor()
    units = processor.emit_multiplication_units(left, right)
    result_blocks = [compute_result_block.s(unit) for unit in units]
    workflow = group(result_blocks) | build_result_matrix.s((left.rows, right.columns))
    return workflow()


@app.task
def compute_result_block(multiplication_unit):
    processor = MatrixProcessor()
    return processor.compute_result_block(*multiplication_unit)


@app.task
def build_result_matrix(computations, result_matrix_size):
    processor = MatrixProcessor()
    return processor.build_result_matrix(computations, result_matrix_size)
