# Calcula #

RESTful service for distributed matrix multiplications.

On this page:

[TOC]

# Try it #

## Using Heroku published version ##

* Install one dependency

    ```
    pip install requests
    ```

* Run demo

    ```
    python demo.py --host https://calcula-demo.herokuapp.com
    ```

_Heroku free instances have resource limitations.
That's why we use small square matrices with size 100 in demo multiplication tasks.
Also in Heroku case task execution time can be very bad and with cold start._

## Using localhost environment ##

* Install RabbitMQ instance
* Install Celery
* Install Calcula requirements

    ```
    pip install -r requirements.txt
    ```

* Run Celery worker

    ```
    celery -A tasks worker
    ```

* Run service

    ```
    python server.py
    ```

* Run demo

    ```
    python demo.py --host http://127.0.0.1:5000
    ```

# Requirements #

+ REST API
    * Create multiplication task
    * Get multiplication task status by id
    * Get multiplication task result by id
* Client demo script
* Python 3.4+
* Without scientific libraries (numpy, panda, etc)

# Analysis #

## Multiplication algorithm ##
There are more than one method to do matrix multiplication.
In this project we will use blocking multiplication.
Approach in which extreme large matrices are interpreted as
having been broken into sections called blocks or sub-matrices.
In this way, multiplication process is transformed into multiplication of blocks.

## Processing workflow ##
Proposed algorithm fit nicely into MapReduce workflow for massively parallel data processing.
Matrix need to be split by block size when parse in mapper.
Mapper must emit a whole row of left matrix blocks and a whole column of right matrix blocks.
First reducer multiplies left and right matrix block pairs and adds up to produce block of result matrix.
Second reducer used to build result matrix.

# Implementation #

## Celery ##
Celery is a task queue based on distributed message passing.
Chord and group primitives can be used to implement MapReduce pattern.

![celery tasks workflow](https://sun9-3.userapi.com/c824701/v824701299/f56d2/8z7Ryiw2JQs.jpg)

```
tasks.py
```

## Model ##

Matrix representation and some operations encapsulated in Matrix class.
We use operator overriding @ for multiplication and + for addition to
support future refactoring of scientific code (using numpy, etc).
MatrixProcessor used to handle distributed matrix multiplication.

```
mathematics.py
```

## Service API ##

Implemented by Flask framework. Reference defined by OpenAPI specification (Swagger).

```
server.py
client.py
```

```yaml

swagger: '2.0'
info:
  version: v1
  title: Calcula API
basePath: /v1
paths:
  /matrix/multiplication/tasks:
    post:
      parameters:
        - in: body
          name: body
          required: true
          schema:
            $ref: '#/definitions/MultiplicationTask'
      responses:
        200:
          description: Matrix multiplication task created and it identifier transmitted in response body.
          schema:
            $ref: '#/definitions/MultiplicationTaskReference'
        400:
          description: Matrix multiplication task not created. Failure reason transmitted in response body.
          schema:
            $ref: '#/definitions/MultiplicationTaskRejection'
  /matrix/multiplication/tasks/{id}/status:
    get:
      parameters:
        - in: path
          name: id
          required: true
          type: string
      responses:
        200:
          description: Matrix multiplication task exist and it status transmitted in response body.
          schema:
            $ref: '#/definitions/MultiplicationTaskStatus'
        404:
          description: Matrix multiplication task with specified id not found.
  /matrix/multiplication/tasks/{id}/result:
    get:
      parameters:
        - in: path
          name: id
          required: true
          type: string
      responses:
        200:
          description: Matrix multiplication task completed and it result transmitted in response body.
          schema:
            $ref: '#/definitions/MultiplicationTaskResult'
        307:
          description: Matrix multiplication task created but not complete yet.
        404:
          description: Matrix multiplication task with specified id not found.
definitions:
  MultiplicationTask:
    type: object
    properties:
      left:
        $ref: '#/definitions/Matrix'
      right:
        $ref: '#/definitions/Matrix'
  MultiplicationTaskRejection:
    type: object
    properties:
      errors:
        type: array
        items:
          type: string
          enum:
            - matrix_unbalanced
            - incorrect_matrix_dimension
  MultiplicationTaskReference:
    type: object
    properties:
      id:
        type: string
  MultiplicationTaskStatus:
    type: object
    properties:
      code:
        type: string
        enum:
          - pending
          - success
  MultiplicationTaskResult:
    type: object
    properties:
      matrix:
        $ref: '#/definitions/Matrix'
  Matrix:
    type: object
    properties:
      rows:
        type: integer
      columns:
        type: integer
      values:
        type: array
        items:
          type: integer

```

# What next ? #

* Current matrix model not memory efficient and produces redundant serializations.
We should use (key, value) notation to map and store.
* In purpose to support extreme large matrices, we should use specific database
or virtual file system instead of Celery backend to store matrix calculation results.
* We need to experiment to find the matrix block size that works best,
as this varies based on network load, multiplication algorithm cpu utilization, size of common matrices.
* Provide service package for simple deploy in runtime environment (Docker).
* Tests.

# References #

* Yunfei Shen and Luyao Li, Matrix Layout Problem in MapReduce
* Wikipedia, Block Matrix
