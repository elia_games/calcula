from flask import Flask, jsonify, abort, request, json

import tasks
from formatters import JsonFormatter
from mathematics import MatrixProcessor

app = Flask(__name__)


@app.route("/", methods=['GET'])
def home():
    return "it works!"


@app.route('/api/v1/matrix/multiplication/tasks', methods=['POST'])
def post_matrix_multiplication_task():
    payload = request.get_json(silent=True)
    formatter = JsonFormatter()
    left = formatter.load_matrix(payload['left'])
    right = formatter.load_matrix(payload['right'])
    processor = MatrixProcessor()
    errors = processor.validate_multiplication(left, right)
    if errors:
        return app.response_class(
            status=400,
            response=json.dumps({'errors': errors}),
            mimetype='application/json'
        )
    result = tasks.multiply_matrices(left, right)
    return jsonify(id=result.id)


@app.route('/api/v1/matrix/multiplication/tasks/<id>/status', methods=['GET'])
def get_matrix_multiplication_task_status(id):
    result = tasks.app.AsyncResult(id)

    # if result.state == tasks.states.PENDING:
    #   return abort(404, description='Matrix multiplication task with specified id not found.')

    return jsonify(code='success' if result.ready() else 'pending')


@app.route('/api/v1/matrix/multiplication/tasks/<id>/result', methods=['GET'])
def get_matrix_multiplication_task_result(id):
    result = tasks.app.AsyncResult(id)

    # if result.state == tasks.states.PENDING:
    #   return abort(404, description='Matrix multiplication task with specified id not found.')

    if not result.ready():
        return abort(307, description='Matrix multiplication task created but not complete yet.')

    formatter = JsonFormatter()
    matrix = formatter.dump_matrix(result.get())
    return jsonify(matrix=matrix)


if __name__ == '__main__':
    app.run(debug=True)
